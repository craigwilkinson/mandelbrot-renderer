SCREEN_WIDTH = 600
SCREEN_HEIGHT = 400

JULIA_WIDTH = 100
JULIA_HEIGHT = 100

ZOOM_SPEED = 1.5

maxIterations = 40

image_counter = 0

_zoom = 1
_x = -0.5
_y = 0

mandeldata = love.image.newImageData(SCREEN_WIDTH + 1, SCREEN_HEIGHT + 1)
juliadata = love.image.newImageData(JULIA_WIDTH + 1, JULIA_HEIGHT + 1)

function log_(n, x)
	return (math.log(x) / math.log(n))
end

function love.load()
	love.graphics.setMode(SCREEN_WIDTH, SCREEN_HEIGHT, false, true, 4)
	if table.getn(arg) > 1 then
		maxIterations = arg[2]
	end
end

function love.keypressed(key, unicode)
	if key == "i" then
		maxIterations = maxIterations + 10
		mandelimage = mandelbrot(_x, _y, _zoom)
	elseif key == "d" then
		maxIterations = maxIterations - 10
		mandelimage = mandelbrot(_x, _y, _zoom)
	elseif key == "r" then
		_x = 0
		_y = 0
		_zoom = 1
		maxIterations = 40
		mandelimage = mandelbrot(_x, _y, _zoom)
	end
end
	
function love.mousepressed(x, y, button)
	_x = mouseX
	_y = mouseY
	_zoom = _zoom * ZOOM_SPEED
	maxIterations = 40 + log_(2, _zoom) * 5
	mandelimage = mandelbrot(_x, _y, _zoom)
end

function love.update()
	-- get the mouse coords
	local unprocessedMouseX = love.mouse.getX()
	local unprocessedMouseY = love.mouse.getY()

	local mandelwidth = 3 / _zoom
	local mandelheight = 2 / _zoom
	local newMouseX = (unprocessedMouseX / SCREEN_WIDTH) * mandelwidth + (_x - mandelwidth / 2)
	local newMouseY = (unprocessedMouseY / SCREEN_HEIGHT) * mandelheight + (_y - mandelheight / 2)

	if newMouseX ~= mouseX or newMouseY ~= mouseY then
		mouseX = newMouseX
		mouseY = newMouseY

		juliaimage = julia(0, 0, 1)
	end

	-- build the images
	if not mandelimage then
		mandelimage = mandelbrot(_x, _y, _zoom)
	end


	if not juliaimage then
		juliaimage = julia(0, 0, 1)
	end

end

function julia(x, y, zoom)
	local width = 2 / zoom
	local height = 2 / zoom
	local left = x - width / 2
	local top = y - height / 2

	for i=0,JULIA_HEIGHT do
		for j=0,JULIA_WIDTH do
			local x = (j / JULIA_WIDTH) * width + left
			local y = (i / JULIA_HEIGHT) * -1 * height + top

			bounded = juliaEscape(x, y, mouseX, mouseY, maxIterations / 2)
			if bounded == -1 then
				juliadata:setPixel(j, i, 0, 0, 0, 255)
			else
				red, green, blue = paletteFromValue(bounded)
				juliadata:setPixel(j, i, red, green, blue, 255)
			end
		end
	end

	return love.graphics.newImage(juliadata)
end


-- draws a mandelbrot at x, y center and zoom level.
-- returns an image
function mandelbrot(x, y, zoom)
	local width = 3 / zoom
	local height = 2 / zoom
	local left = x - width / 2
	local top = y - height / 2

	for i=0,SCREEN_HEIGHT do
		for j=0,SCREEN_WIDTH do
			local x = (j / SCREEN_WIDTH) * width + left
			local y = (i / SCREEN_HEIGHT) * height + top

			bounded = mandelbrotEscape(x, y, maxIterations)
			if bounded == -1 then
				mandeldata:setPixel(j, i, 0, 0, 0, 255)
			else
				red, green, blue = paletteFromValue(bounded)
				mandeldata:setPixel(j, i, red, green, blue, 255)
			end
		end
	end

	print("Iter: ", maxIterations, "\nZoom: ", _zoom, "\n", _x, ", ", _y, "MyIter: ", log_(2, _zoom))
	mandeldata:encode(image_counter .. ".png")
	image_counter = image_counter + 1
	return love.graphics.newImage(mandeldata)
end

function mandelbrotEscape(real, im, maxIterations)
	return mandelbrotIterator(0, 0, real, im, maxIterations)
end

function juliaEscape(zRealStart, zImStart, cReal, cIm, iterations)
	return mandelbrotIterator(zRealStart, zImStart, cReal, cIm, iterations)
end

function mandelbrotIterator(zRealStart, zImStart, cReal, cIm, maxIterations)
	local zReal, zIm = zRealStart, zImStart
	for i=0,maxIterations do
		zRealSquared = zReal ^ 2
		zImSquared = zIm ^ 2

		magnitude = (zRealSquared + zImSquared)
		if magnitude > 4 then
			return i + 1 - log_(2, (log_(2, (magnitude)) / 2))
		end

		zReal, zIm = zRealSquared - zImSquared, 2 * zReal * zIm
		zReal, zIm = zReal + cReal, zIm + cIm 
	end

	return -1
end

-- returns a red green blue result from the value (between 0 and 1)
function paletteFromValue(value)
	local red = (math.sin(value * 0.1) + 1) / 2 * 255
	local green = (math.cos(value * 0.1) + 1) / 2 * 100
	local blue = (math.cos(value * 0.1) + 1) / 2 * 255

	return red, green, blue
end

function complexSquare(real, complex)
	return real * real - complex * complex, 2 * real * complex
end

function love.draw()
	love.graphics.draw(mandelimage)
	--love.graphics.draw(juliaimage)
	love.timer.sleep(0.2)
end
